<?php

require __DIR__ . '/Exceptions/FileNotFoundException.php';

class HtmlParser {

    public function parse(string $filename): array
    {
        if (!is_file($filename)) {
            throw new FileNotFoundException();
        }
        $string = file_get_contents($filename);

        preg_match_all("/<a .*href=\"(https?:\/\/.{2,})\"/",$string, $matches);

        return array_unique($matches[1]);
    }
}