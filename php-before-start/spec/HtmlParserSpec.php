<?php namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class HtmlParserSpec extends ObjectBehavior
{
    public function getMatchers(): array
    {
        return [
            'haveEqualContent' => function (array $result, array $expectedResult) {
                sort($result);
                sort($expectedResult);
                return $result === $expectedResult;
            }
        ];
    }

    function it_get_links_1 () {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'test1.html';
        $this->parse($filePath)->shouldHaveEqualContent(['https://akdev.by']);
    }

    function it_get_links_2 () {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'test2.html';
        $this->parse($filePath)->shouldHaveEqualContent([
            'https://test.by',
            'http://tes1.by'
        ]);
    }

    function it_get_links_3 () {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'test3.html';
        $this->parse($filePath)->shouldHaveEqualContent([
            'https://test13.com',
            'http://test13.com',
            'http://test1.com',
            'https://test7.com',
        ]);
    }

    function it_get_links_4 () {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'test4.html';
        $this->parse($filePath)->shouldHaveEqualContent([]);
    }

    function it_takes_exception_1()
    {
        $this->shouldThrow('FileNotFoundException')->duringParse('abracadabra.html');
    }
}