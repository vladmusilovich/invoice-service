var gulp = require('gulp');
var phpspec = require('gulp-phpspec');
var run = require('gulp-run');
var notify = require('gulp-notify');

// uncomment this part for unix system
gulp.task('test', function() {
    gulp.src('spec/**/*.php')
        .pipe(phpspec('', { 'verbose': 'v', notify: true }))
        .on('error', notify.onError({
            title: "Crap",
            message: "Your tests failed, Jeffrey!",
            icon: __dirname + '/fail.png'
        }))
        .pipe(notify({
            title: "Success",
            message: "All tests have returned green!"
        }));
});

// uncomment this part for windows system
// gulp.task('test', function(){
//   run('clear', {'usePowerShell': true}).exec('Vinyl', function(){
//     gulp.src('spec/**/*.php')
//       .pipe(phpspec('', { notify: true}))
//       .on('error', notify.onError({
//         title: "Crap",
//         message: "Your tests failed, Jeffrey!",
//         icon: __dirname + '/fail.png'
//       }))
//       .pipe(notify({
//         title: "Success",
//         message: "All tests have returned green!"
//       }));
//   });
// });

gulp.task('watch', function() {
    gulp.watch(['spec/**/*.php', 'src/**/*.php'], ['test']);
});

gulp.task('default', ['test', 'watch']);