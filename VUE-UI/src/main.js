import Vue from 'vue'
import App from './App.vue'
import style from './assets/slyle/main.sass'
import router from './router/index'
import store from './store/index'
import Toasted from 'vue-toasted'

Vue.use(Toasted)

new Vue({
    el: '#app',
    style,
    router,
    store,
    render: h => h(App)
})
