export default [
    {
        number: 1,
        name: 'Start',
        description: 'To start editing your invoice,<br>' +
        '                    you must first sign up or login',
        sideBlock: true,
        colorClass: 'step_color_blue',
        lineDownHeight: 'step__line-down_height_large',
        lineLeft: false
    },
    {
        number: 2,
        name: 'Step',
        description: 'You need to enter invoice-section<br>' +
        '                    number, date, "from" and "for"<br>' +
        '                    this invoice-section',
        sideBlock: true,
        colorClass: 'step_color_dark-green',
        lineDownHeight: 'step__line-down_height_big',
        lineLeft: true
    },
    {
        number: 3,
        name: 'Step',
        description: 'You can write notes to your<br>' +
        '                    invoice-section, but it is not necessary',
        sideBlock: true,
        colorClass: 'step_color_green',
        lineDownHeight: 'step__line-down_height_medium',
        lineLeft: true
    },
    {
        number: 4,
        name: 'Step',
        description: 'Edit your payment table',
        sideBlock: true,
        colorClass: 'step_color_yellow',
        lineDownHeight: 'step__line-down_height_small',
        lineLeft: true
    },
    {
        number: 5,
        name: 'Final',
        description: 'Send your invoice-section or save to<br>' + 'pdf',
        sideBlock: false,
        colorClass: 'step_color_red',
        lineLeft: true
    }
]