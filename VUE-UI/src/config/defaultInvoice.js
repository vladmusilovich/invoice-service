export default {
    number: "",
    date: "yyyy-MM-dd",
    user: {
        name: "",
        email: "",
        address: ""
    },
    client: {
        name: "",
        email: "",
        address: ""
    },
    notes: "",
    expenses: [],
    total: ""
}