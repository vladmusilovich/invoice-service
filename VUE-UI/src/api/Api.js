import axios from 'axios'
import _ from 'lodash'

class Api {
    constructor() {
        this.axios = axios.create({
            headers: {}
        })
    }

    setToken (token) {
        if (token) {
            this.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        } else {
            delete this.axios.defaults.headers.common['Authorization'];
        }
    }
}

export default new Api()