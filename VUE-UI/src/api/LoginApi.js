import api from './Api'

class LoginApi {

    static login (data) {
        return api.axios.post('/api/login', data);
    }
}

export default LoginApi