import api from './Api'

class UserApi {

    static show () {
        return api.axios.get('/api/user');
    }

    static store (data) {
        return api.axios.post('/api/signup', data);
    }
}

export default UserApi