import api from './Api.js'

class InvoiceApi {

    static index () {
        return api.axios.get('/api/invoice');
    }

    static show (id) {
        return api.axios.get('/api/invoice/' + id);
    }

    static store (data) {
        return api.axios.post('/api/invoice', data);
    }

    static update (id, data) {
        return api.axios.put('/api/invoice/' + id, data)
    }

    static deleteInvoice (id) {
        return api.axios.delete('/api/invoice/' + id);
    }
}

export default InvoiceApi