import Vue from 'vue'
import VueRouter from 'vue-router'

import Homepage from '../components/layouts/AppBody.vue'
import NotFound from '../components/errors/NotFound.vue'
import Settings from '../components/pages/Settings.vue'
import InvoiceEditor from '../components/pages/InvoiceEditor.vue'
import TableInvoices from '../components/pages/TableInvoices.vue'
import CurrencyConverter from "../components/pages/CurrencyConverter.vue"

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'homepage',
        component: Homepage
    },
    {
        path: '/invoice-settings',
        name: 'settings',
        component: Settings
    },
    {
        path: '/currency-converter',
        name: 'currency converter',
        component: CurrencyConverter
    },
    {
        path: '/all-invoices',
        name: 'all-invoices',
        component: TableInvoices
    },
    {
        path: '/invoice/:invoiceId?',
        name: 'invoice editor',
        component: InvoiceEditor,
    },
    {
        path: '*',
        name: 'not found',
        component: NotFound
    }
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

export default router