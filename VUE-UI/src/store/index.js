import Vue from 'vue'
import Vuex from 'vuex'
import Api from '../api/Api.js'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        currentCurrency: 'USD',
        totalOfAmount: 0,
        userToken: '',
        invoice:  {},
    },
    actions: {
        setTotalOfAmount({commit}, total) {
            commit('setTotalOfAmount', total);
        },
        setCurrentCurrency({commit}, currency) {
            commit('setCurrentCurrency', currency);
        },
        setInvoice({commit}, invoice) {
            commit('setInvoice', invoice);
        },
        setUserToken({commit}, token) {
            commit('setUserToken', token);
            Api.setToken(token);
            let date = new Date();
            date.setTime(date.getTime() + (24*60*60*1000));
            let expires = 'expires=' + date.toUTCString();
            document.cookie = 'userToken=' + token + ';' + expires + ';';
        }
    },
    mutations: {
        setTotalOfAmount (state, total) {
            state.totalOfAmount = total;
        },
        setCurrentCurrency (state, currency) {
            state.currentCurrency = currency;
        },
        setInvoice (state, invoice) {
            state.invoice = invoice;
        },
        setUserToken(state, token) {
            state.userToken = token;
        },
        setUser(state, user) {
            state.user = user;
        }
    }
});

export default store