<?php

namespace App\Models;

use App\DB\Connection;
use App\Exceptions\ModelNotFoundException;
use PDO;

abstract class Model {

    protected $id;
    protected $connection;
    protected $attributes;
    protected $isLoaded;

    public function __construct($id = null)
    {
        $this->id = $id;
        $this->connection = Connection::getInstance();
    }

    public function __get($attribute)
    {
        if ($attribute === 'attributes') {
            return $this->attributes;
        }
        return $this->attributes->$attribute;
    }

    public function fill($attributes)
    {
        $this->attributes = $attributes;
        if(isset($attributes->id)) {
            $this->id = $attributes->id;
        }
    }

    public function load()
    {
        $statement = $this->connection->getConnection()->prepare(
            'SELECT * FROM ' . $this->table . ' WHERE id = :id LIMIT 1'
        );
        $statement->bindParam(':id', $this->id, PDO::PARAM_STR);
        $statement->execute();
        $this->attributes = $statement->fetch(PDO::FETCH_OBJ);
        if (!$this->attributes || !isset($this->attributes) || $this->attributes === []) {
            throw new ModelNotFoundException($this->id, $this->table);
        }
        $this->attributes = $this->castAttributes($this->attributes);
        $this->isLoaded = true;
    }

    public function save()
    {
        $keysArray = array_keys(get_object_vars($this->attributes));
        if (!$this->isLoaded || !$this->id) {
            $this->id = uniqid();
            $this->attributes->id = $this->id;
            $keysString = implode(',', $keysArray);
            $bindKeysString = ':' . str_replace(',', ',:', $keysString);
            $statement = $this->connection->getConnection()->prepare(
                "INSERT INTO $this->table ($keysString) VALUES ($bindKeysString)"
            );
        } else {
            foreach ($keysArray as $index => $key) {
                if ($key === 'id') {
                    unset($keysArray[$index]);
                } else {
                    $keysArray[$index] = $key . ' = :' . $key . ', ';
                }
            }
            $expression = trim(implode($keysArray), ', ');
            $statement = $this->connection->getConnection()->prepare(
                "UPDATE $this->table SET $expression WHERE id = :id"
            );

            $keysArray = array_keys(get_object_vars($this->attributes));
        }

        foreach ($keysArray as $index => $key) {
            if (in_array($key, $this->casts)) {
                $statement->bindValue(':' . $key, json_encode($this->attributes->$key), PDO::PARAM_STR);
            } else {
                $statement->bindValue(':' . $key, $this->attributes->$key, PDO::PARAM_STR);
            }
        }

        $statement->execute();
    }

    public function delete()
    {
        $statement = $this->connection->getConnection()->prepare("DELETE FROM $this->table WHERE id = :id");
        $statement->bindValue(':id', $this->id, PDO::PARAM_STR);
        $statement->execute();
    }

    public function toJSON()
    {
        return json_encode($this->attributes);
    }

    public function castAttributes($attributes)
    {
        foreach ($attributes as $field => $value) {
            if (is_string($value) && in_array($field, $this->casts)) {
                $attributes->$field = json_decode($value);
            }
        }
        return $attributes;
    }

    public function setUserId($userId)
    {
        $this->attributes->userId = $userId;
    }
}