<?php

namespace App\Models;

use App\DB\Connection;
use App\Exceptions\SignUpException;

class User extends Model
{
    protected $err = null;
    protected $table = 'users';

    public function validate() {

        if(!preg_match("/^[a-zA-Z0-9]+$/", $this->attributes->login)) {
            $this->err = "Please use the letters of the English alphabet and numbers";
            throw new SignUpException($this->err, 401);
        }

        if(strlen($this->attributes->login) < 3 or strlen($this->attributes->login) > 30) {
            $this->err = "Login must be at least 3 characters and not more than 30";
            throw new SignUpException($this->err, 401);
        }

        if(!$this->isNew()) {
            $this->err = "A user with such a login already exists in the database";
            throw new SignUpException($this->err, 401);
        }
    }

    private function isNew(): bool
    {
        $pdo = Connection::getInstance();
        $users = $pdo->getConnection()->query('SELECT * FROM users')->fetchAll();
        foreach ($users as $user) {
            if ($user['login'] == $this->attributes->login) {
                return false;
            }
        }
        return true;
    }
}