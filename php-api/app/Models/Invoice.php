<?php

namespace App\Models;

class Invoice extends Model {

    protected $table = 'invoices';
    protected $casts = ['expenses', 'user', 'client'];
}