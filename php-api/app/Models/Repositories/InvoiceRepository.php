<?php

namespace App\Models\Repositories;

use App\DB\Connection;
use App\Models\Invoice;
use PDO;

class InvoiceRepository
{
    protected $connection;
    protected $table = 'invoices';
    protected $modelClass = Invoice::class;

    public function __construct()
    {
        $this->connection = Connection::getInstance();
    }

    public function getAllInvoices($userId)
    {
        $responseBody = [];
        $connection = Connection::getInstance();
        $idList = $connection->getConnection()->prepare(
            "SELECT id FROM $this->table where userId = :userId"
        );
        $idList->bindValue(':userId', $userId, PDO::PARAM_STR);
        $idList->execute();
        $idList = $idList->fetchAll(PDO::FETCH_OBJ);

        foreach ($idList as $i => $id) {
            $instance = new $this->modelClass($id->id);
            $instance->load();
            $responseBody[] = $instance->attributes;
        }
        return $responseBody;
    }
}