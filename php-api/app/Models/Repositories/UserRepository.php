<?php

namespace App\Models\Repositories;

use App\DB\Connection;
use App\Models\User;
use PDO;

class UserRepository
{
    public function getUserByLogin($login)
    {
        $pdo = Connection::getInstance();
        $statement = $pdo->getConnection()->prepare('SELECT * FROM users WHERE login = :login');
        $statement->bindParam(':login', $login, PDO::PARAM_STR);
        $statement->execute();

        $userObj = $statement->fetch(PDO::FETCH_OBJ);
        $user = new User($userObj->id);
        $user->fill($userObj);
        return $user;
    }

}