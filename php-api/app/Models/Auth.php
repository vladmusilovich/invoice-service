<?php

namespace App\Models;

use App\Exceptions\InvalidTokenException;
use App\Http\Requests\Request;
use Firebase\JWT\JWT;

class Auth
{
    const USER_ID_SESSION_KEY = 'user_id';
    const AUTHORIZATION_TYPE = 'Bearer';

    protected $user;
    public static $instance = null;

    public function __construct () {}
    private function __clone () {}

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public static function getUserByToken($token) : User
    {
        if(!($token === 'Bearer')){
            $authArray = JWT::decode($token, getenv('APP_KEY'), ['HS256']);
            $user = new User($authArray->user_id);
            $user->load();
            return $user;
        }
        $user = new User();
        return $user;
    }

    public static function getTokenByUser(User $user) : string
    {
        return JWT::encode([self::USER_ID_SESSION_KEY => $user->id], getenv('APP_KEY'));
    }

    public function authByRequest(Request $request) : void
    {
        $header = $request->getHeader('authorization');
        $token = str_replace(self::AUTHORIZATION_TYPE . ' ', '', $header);

        $this->setUser(self::getUserByToken($token));
    }

    public function setUser($user) : void
    {
        $this->user = $user;
    }

    public function getUser() : User
    {
        return $this->user;
    }
}