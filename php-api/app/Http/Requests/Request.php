<?php

namespace App\Http\Requests;

use App\Http\Router;

class Request
{
    protected $serverParams;
    protected $middlewares = [];
    protected $route;
    protected $body;
    protected $headers = [];

    public function __construct($serverParams, $body)
    {
        $this->body = $body;
        $this->serverParams = $serverParams;
        $this->headers = getallheaders();
        $router = new Router();
        $this->route = $router->getRoute($this);
        $this->initMiddlewares();
    }

    public static function createFromGlobals(array $globals)
    {
        $body = file_get_contents('php://input');
        return new static($globals, $body);
    }

    public function getServerParams()
    {
        return $this->serverParams;
    }

    public function getServerParam($key, $default = NULL)
    {
        $serverParams = $this->getServerParams();
        return isset($serverParams[$key]) ? $serverParams[$key] : $default;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getHeader($key)
    {
        return $this->headers[$key];
    }

    public function getJSONParsedBody()
    {
        return json_decode($this->getBody());
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function getMiddlewares()
    {
        return $this->middlewares;
    }

    protected function initMiddlewares()
    {
        foreach ($this->route['middleware'] as $middleware) {
            $this->middlewares[] = new $middleware();
        }
    }
}