<?php

namespace App\Http;

use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\UserController;

use App\Http\Requests\Request;
use UnexpectedValueException;

class Router {

    protected $routes = [

        'GET' => [
            '/invoice' => 'InvoiceController@index',
            '/invoice/{id}' => 'InvoiceController@show',
            '/user' => 'UserController@show'
        ],

        'POST' => [
            '/invoice' => 'InvoiceController@store',
            '/signup' => 'UserController@store',
            '/login' => 'LoginController@login'
        ],

        'PUT' => [
            '/invoice/{id}' => 'InvoiceController@update'
        ],

        'DELETE' => [
            '/invoice/{id}' => 'InvoiceController@delete'
        ]
    ];

    public function getRoute(Request $request)
    {
        $method = $request->getServerParam('REQUEST_METHOD');
        $routes = $this->routes[$method];
        $requestUri = $request->getServerParam('REQUEST_URI');

        foreach ($routes as $patternUri => $action) {

            if (($params = $this->getRequestParams($requestUri, $patternUri)) !== null) {
                list($controllerName, $methodName) = explode('@', $action);

                $controllerName = 'App\Http\Controllers\\' . $controllerName;
                $controller = new $controllerName($request);
                return [
                    'middleware' => $controller->getMiddlewares(),
                    'action' => function () use ($methodName, $controller, $params) {
                        return call_user_func_array([$controller,$methodName], $params);
                    }
                ];
            };
        }
        return false;
    }

    public function getRequestParams($requestUri, $patternUri)
    {
        $requestSegments = explode('/', $requestUri);
        $patternSegments = explode('/', $patternUri);

        if(count($requestSegments) !== count($patternSegments)) {
            return null;
        }

        $params = [];
        foreach ($patternSegments as $key => $patternSegment) {

            $requestSegment = $requestSegments[$key];

            if($patternSegment === $requestSegment) {
                continue;
            }

            if(substr($patternSegment, 0,1) === '{' && substr($patternSegment,-1) === '}') {
                $params[] = $requestSegment;
                continue;
            }
            return null;
        }
        return $params;
    }
}