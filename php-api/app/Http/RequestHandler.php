<?php

namespace App\Http;

use App\Http\Requests\Request;

class RequestHandler
{
    private $callback;

    public function __construct($callback)
    {
        $this->callback = $callback;
    }
    /**
     * Handle the request and return a response.
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request)
    {
        return call_user_func($this->callback, $request);
    }
}