<?php

namespace App\Http;

use App\Http\Middlewares\RouteHandler;
use UnexpectedValueException;
use App\Http\Requests\Request;
use App\Http\Middlewares\Authenticate;

class Dispatcher
{
    protected $stack;

    public function __construct(array $stack)
    {
        $this->stack = $stack;
    }

    public function dispatch(Request $request)
    {
        $resolved = $this->resolve(0);
        return $resolved->handle($request);
    }

    private function resolve($index)
    {
        return new RequestHandler(function (Request $request) use ($index) {

            $middleware = $this->stack[$index];
            $response = $middleware->process($request, $this->resolve($index + 1));

            return $response;
        });
    }
}