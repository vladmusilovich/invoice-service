<?php

namespace App\Http;

use App\Http\Middlewares\RouteHandler;
use App\Http\Requests\Request;
use UnexpectedValueException;

class Kernel
{
    protected $globalMiddlewares = [];

    protected $request;

    public function __construct()
    {
        $this->request = Request::createFromGlobals($_SERVER);
        $this->globalMiddlewares = $this->request->getMiddlewares();
        $this->globalMiddlewares[] = new RouteHandler();
    }

    public function run()
    {
        $dispatcher = new Dispatcher($this->globalMiddlewares);
        $response = $dispatcher->dispatch($this->request);

        if (!($response instanceof Response)) {
            throw new UnexpectedValueException();
        }
        $this->respond($response);
    }

    public function respond(Response $response)
    {
        http_response_code($response->getStatusCode());
        foreach ($response->getHeaders() as $header) {
            header($header);
        }
        printf($response->getBody());
    }
}