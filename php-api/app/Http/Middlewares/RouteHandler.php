<?php

namespace App\Http\Middlewares;


use App\Http\RequestHandler;
use App\Http\Requests\Request;

class RouteHandler implements MiddlewareInterface
{
    public function process(Request $request,  RequestHandler $handler)
    {
        $callback = $request->getRoute()['action'];
        return call_user_func($callback); // Response
    }
}