<?php

namespace App\Http\Middlewares;

use App\Http\RequestHandler;
use App\Http\Requests\Request;

interface MiddlewareInterface {

    public function process(Request $request, RequestHandler $handler);
}