<?php
namespace App\Http\Middlewares;

use App\Exceptions\InvalidTokenException;
use App\Http\RequestHandler;
use App\Http\Requests\Request;
use App\Models\Auth;

class Authenticate implements MiddlewareInterface
{
    public function process(Request $request, RequestHandler $handler)
    {
        try {
            $auth = Auth::getInstance();
            $auth->authByRequest($request);
            return $handler->handle($request);
        } catch (InvalidTokenException $exception) {
            throw new $exception;
        }
    }
}