<?php

namespace App\Http\Controllers;

use App\Exceptions\WrongLoginException;
use App\Exceptions\WrongPasswordException;
use App\Models\Auth;
use App\Http\Requests\Request;
use App\Http\Response;
use App\Models\Repositories\UserRepository;
use App\Models\User;

class LoginController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function login()
    {
        $login = $this->request->getJSONParsedBody()->login;
        $password = $this->request->getJSONParsedBody()->password;

        $repository = new UserRepository();
        $user = $repository->getUserByLogin($login);

        if(!($user instanceof User) || !$user->attributes) {
            throw new WrongLoginException($login, 401);
        }

        if(!($password === $user->password)) {
            throw new WrongPasswordException($login, 401);
        }

        return new Response(200, [], Auth::getTokenByUser($user));
    }
}