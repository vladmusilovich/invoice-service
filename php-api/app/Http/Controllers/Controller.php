<?php

namespace App\Http\Controllers;
use App\Http\Requests\Request;
use App\Models\Auth;

class Controller
{
    protected $request;
    protected $middlewares = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->auth = Auth::getInstance();
    }

    public function getMiddlewares()
    {
        return $this->middlewares;
    }

    protected function addMiddleware(string $className)
    {
        $this->middlewares[] = $className;
    }
}