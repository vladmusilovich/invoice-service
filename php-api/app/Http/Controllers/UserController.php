<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Http\Response;
use App\Models\Auth;
use App\Models\User;


class UserController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function store()
    {
        $user = new User();
        $user->fill($this->request->getJSONParsedBody());
        $user->validate();
        $user->save();

        return new Response(200, [], ['status' => 'ok']);
    }

    public function show()
    {
        $auth = new Auth();
        $auth->authByRequest($this->request);
        $user = $auth->getUser();

        if(!$user->attributes) {
            return new Response(200, [], '');
        }

        return new Response(200, ['Content-type: application/json'], $user->toJSON());
    }
}