<?php

namespace App\Http\Controllers;

use App\Http\Middlewares\Authenticate;
use App\Http\Requests\Request;
use App\Http\Response;
use App\Models\Invoice;
use App\Models\Repositories\InvoiceRepository;

class InvoiceController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->addMiddleware(Authenticate::class);
    }

    public function index()
    {
        $repository = new InvoiceRepository();
        return new Response(200, ['Content-type: application/json'], json_encode(
            $repository->getAllInvoices($this->auth->getUser()->id)
        ));
    }

    public function show($id)
    {
        $invoice = new Invoice($id);
        $invoice->load();

        if ($invoice->userId !== $this->auth->getUser()->id) {
            return new Response(403, [], '');
        }

        return new Response(200, ['Content-type: application/json'], $invoice->toJSON());
    }

    public function store()
    {
        $invoice = new Invoice();
        $invoice->fill($this->request->getJSONParsedBody());
        $userId = $this->auth->getUser()->id;
        $invoice->setUserId($userId);
        $invoice->save();

        return new Response(200, ['Content-type: application/json'],  $invoice->toJSON());
    }

    public function update($id)
    {
        $invoice = new Invoice($id);
        $invoice->load();
        if ($invoice->userId !== $this->auth->getUser()->id) {
            return new Response(403, [], '');
        }
        $invoice->fill($this->request->getJSONParsedBody());
        $invoice->save();
        return new Response(200, ['Content-type: application/json'], $invoice->toJSON());
    }

    public function delete($id)
    {
        $invoice = new Invoice($id);
        $invoice->delete();
        return new Response(200, ['Content-type: application/json'], json_encode(['status' => 'ok']));
    }
}