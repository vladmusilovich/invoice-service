<?php

namespace App\DB;

use PDO;

class Connection
{
    private static $instance = null;
    private $conn;

    private function __construct () {

        $this->conn = new PDO(
            'mysql:dbname=' . $_ENV['DB_DATABASE'] . ';' .
            'host=' . $_ENV['DB_HOST'] . ';' .
            'port=' . $_ENV['DB_PORT'] . ';' ,
            $_ENV['DB_USERNAME'],
            $_ENV['DB_PASSWORD'],
            [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'", PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]
        );
    }

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Connection();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->conn;
    }
}