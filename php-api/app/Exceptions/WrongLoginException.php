<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class WrongLoginException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('User ' . $message . ' does not exist.', $code, $previous);
    }
}