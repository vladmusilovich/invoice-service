<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class WrongPasswordException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Wrong password for user ' . $message . '.', $code, $previous);
    }
}