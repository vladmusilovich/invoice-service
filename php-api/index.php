<?php

use App\Http\Kernel;
use Symfony\Component\Dotenv\Dotenv;

require 'vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->load('.env');
$httpKernel = new Kernel();
$httpKernel->run();